<?php
    session_start();
?>
<?php
    $isSet = false;
    $username = $_SESSION["username"];

    if(isset($_POST['Logout'])){
        session_destroy();
        echo'Sei stato disconnesso';
    }

    if(!isset($_SESSION["username"])){
       header("location:./login.php");
    }

    $fullname = $_POST["fullname"];
    $data = $_POST['data'];
    $telefono = $_POST["telefono"];
    $email = $_POST["email"];
    $gender = $_POST["gender"];
    $text = $_POST['text'];
?>  
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="style3.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curriculum</title>
</head>
<body>
<form method="post">
<nav class="navbar navbar-light" style="background-color: #1abc9c;">
    <div>
        <input type="submit" class="btn btn-primary" name="Logout" value ="Logout">
        <?php
            echo "Benvenuto, $username";
        ?>
    </div>
</nav>
<h3>Dati del candidato:</h3>
<div class="container">
    <div class="row">
        <div class="col sx">
            <center>
            <img src="./img.jpg" alt=""width="200" height=auto>
            <br>
            <p>foto candidato</p>
            </center>
        </div>
        <div class="col dx">
            <span><bold>Nome e Cognome:</bold></span>
            <?php echo "$fullname" ?>
            <br>
            <span><bold>Sesso:</bold></span>
            <?php echo "$gender" ?>
            <br>
            <span><bold>Data di Nascita:</bold></span>
            <?php echo "$data" ?>
            <br>
            <span><bold>Telefono:</bold></span>
            <?php echo "$telefono" ?>
            <br> 
            <span><bold>Breve racconto delle tue esperienze:</bold</span>
            <br>
            <?php echo "$text" ?>
            <br>
        </div>  
    </div>
</div>
</form>
</body>
</html>