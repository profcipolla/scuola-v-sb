<?php
    session_start();

    if (isset($_GET['logout'])) {
        session_destroy();
        header('location:login.php');
        exit();
    }

    if(isset($_POST['invia'])){
        header('location:curriculum.php');
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BENVENUTO</title>
    <link rel="stylesheet" type="text/css" href="style2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"/>
</head>
<body>
<form action="curriculum.php" method="post">
<div class="container">
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Nome completo:</label>
                <input class="form-control" type="text" name="fullname" required placeholder="Inserisci il tuo nome e cognome"/>
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input class="form-control" type="email" name="email"  required placeholder="Inserisci l'email"/>
            </div>
            <div class="form-group">
                <label>Telefono:</label>
                <input class="form-control" type="text" name="telefono" required placeholder="Inserisci il tuo numero"/>
            </div>
        </div> 
        <div class="col">  
            <div class="form-group">
                <label for="date2">Data di Nascita</label>
                <input class="form-control it-date-datepicker" id="data" name ="data"type="text" placeholder="inserisci la data in formato gg/mm/aaaa">
            </div>
            <div class="form-group">
                <label>Sesso</label><br/>
                <label><input type="radio" name="gender" required value="Maschio" checked /> Maschio</label>
                <label><input type="radio" name="gender" required value="Femmina" /> Femmina</label>
                <label><input type="radio" name="gender" required value="Altro" /> Altro</label>
            </div>
            <div class="form-group">
                <textarea id="exampleFormControlTextarea1" name ="text" rows="3"></textarea>
                <label> Breve descrizione </label>
            </div>
            <div class="form-group">
                <input class="btn btn-primary btn-block" name ="invia" type="submit" value="Invia"./>
            </div>
        </div>
    </div>
</div>
</form>

</body>
</html>  






