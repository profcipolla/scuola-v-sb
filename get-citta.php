<?php
    // cross origin
    header("Access-Control-Allow-Origin: *");
    
    // la pagina restituisce json
    header("Content-Type: application/json; charset=UTF-8");

    $citta = Array();
    $citta["citta"] = Array();


    $roma = Array();
    $roma["id"] = "001";
    $roma["nome"] = "Roma";

    $frosinone = Array();
    $frosinone["id"] = "002";
    $frosinone["nome"] = "Frosinone";

    array_push($citta["citta"], $roma);
    array_push($citta["citta"], $frosinone);

    // stato http
    http_response_code(200);

    echo json_encode($citta);