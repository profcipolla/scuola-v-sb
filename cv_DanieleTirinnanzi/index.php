<?php session_start() ?>
<html>

<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <style>
    body {
    margin: 0;
    padding: 0;
    background-color: #17a2b8;
    height: 100vh;
  }
  #login .container #login-row #login-column #login-box {
    margin-top: 120px;
    max-width: 600px;
    height: 320px;
    border: 1px solid #9C9C9C;
    background-color: #EAEAEA;
  }
  #login .container #login-row #login-column #login-box #login-form {
    padding: 20px;
  }
  #login .container #login-row #login-column #login-box #login-form{
    margin-top: -85px;
  }
  #register-link{
    margin-top: -30px;
  }
  </style>
</head>

<body>
  <?php
  $author = "Daniele Tirinnanzi";
  $erroreLogin = "";
  $username = "";

  if(isset($_POST["login"])){

    unset($_SESSION["username"]);
    $username = $_POST["username"];
    $password = $_POST["password"];

    if($username == $password){ //PROVVISORIO
      $_SESSION["username"] = $username;
      header("location: /cv_DanieleTirinnanzi/formCV.php");
      //header("location: /cv/formCV.php");
    }else{
      $erroreLogin = "Nome utente o password errati!";
    }
  }
   ?>
   <form method="post">
   <div id="login">
       <div class="container">
           <div id="login-row" class="row justify-content-center align-items-center">
               <div id="login-column" class="col-md-6">
                   <div id="login-box" class="col-md-12">
                       <form id="login-form" class="form" action="" method="post">
                           <h3 class="text-center text-info">Login</h3>
                           <div class="form-group">
                               <label for="username" class="text-info">Username:</label><br>
                               <input type="text" name="username" id="username" class="form-control" value= <?=$username;?> >
                           </div>
                           <div class="form-group">
                               <label for="password" class="text-info">Password:</label><br>
                               <input type="password" name="password" id="password" class="form-control">
                           </div>
                           <div class="form-group">
                               <label for="remember-me" class="text-info"><span>Ricordami</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br>
                               <input type="submit" name="login" class="btn btn-info btn-md" value="Accedi">
                               <input type="reset" class="btn btn-secondary btn-md" value="Cancella">
                           </div>
                           <div id="register-link" class="text-right">
                               <input type="button" onclick="location.href='https://www.google.it';" class="btn btn-light" value="Registrati!"/>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
 </form>
 
 <?php
  if($erroreLogin){
    echo '<div class="alert alert-danger" role="alert" style="width:20%;">';
    echo $erroreLogin;
    echo "</div>";
  }
  ?>
</body>

 </html>
