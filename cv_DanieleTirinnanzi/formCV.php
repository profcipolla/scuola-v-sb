<?php session_start() ?>

<?php
  $nome = "";
  $cognome = "";
  $email = "";
  $telefono = "";
  $indirizzo = "";
  $descrizione = "";
  $isAutenticato = false;
  if(isset($_POST["invia"])){
    $nome = $_POST["nome"];
    $cognome = $_POST["cognome"];
    $email = $_POST["email"];
    $telefono = $_POST["telefono"];
    $indirizzo = $_POST["indirizzo"];
    $descrizione = $_POST["descrizione"];
    if (!("" == $nome or "" == $cognome or "" == $email or "" == $telefono)){
      $_SESSION["nome"] = $nome;
      $_SESSION["cognome"] = $cognome;
      $_SESSION["telefono"] = $telefono;
      $_SESSION["email"] = $email;
      $_SESSION["indirizzo"] = $indirizzo;
      $_SESSION["sesso"] = $_POST["sesso"];
      $_SESSION["descrizione"] = $_POST["descrizione"];
      header("location: /cv_DanieleTirinnanzi/curriculum.php");
    }
  }

  if(isset($_SESSION["username"])){
    $isAutenticato = true;
    $username = $_SESSION["username"];
  }
  if(isset($_GET["logout"])){
    session_destroy();
    header("location: /cv_DanieleTirinnanzi/index.php");
  }

 ?>

<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <style>

  body{
    margin:20px;
  }
  .top{
    margin-left:-20px;
    margin-right:-20px;
    margin-top:-20px;
    padding:20px;
    background-color: #4bcffa;
    margin-bottom:5%;
  }
  .top #logout{
    margin-top: -5px;
    float:right;
  }

  </style>

  <title>
    Dashboard
  </title>

</head>

<body>

  <form method="get">
  <div class="top">
    <strong>Benvenuto <?=$username; ?></strong>
    <input type="submit" name="logout" id="logout" value="Logout">
  </div>
  </form>

  <?php
    if(!$isAutenticato){
      header("location: /cv_DanieleTirinnanzi/index.php");
    }
   ?>

   <form method="post">
  <div class="form-group">
    <label for="mail">*Indirizzo e-mail</label>
    <input type="email" name="email" class="form-control" id="email" value="<?=$email;?>" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">Condivideremo a tutti la tua email</small>
  </div>
  <div class="form-group">
    <label for="nome">*Nome</label>
    <input type="text" name="nome" class="form-control" id="nome" value="<?=$nome;?>">
  </div>
  <div class="form-group">
    <label for="cognome">*Cognome</label>
    <input type="text" name="cognome" class="form-control" id="cognome" value="<?=$cognome;?>">
  </div>
  <div class="form-group">
    <label for="telefono">*Telefono</label>
    <input type="text" class="form-control" name="telefono" id="telefono" value="<?=$telefono;?>" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label for="indirizzo">Indirizzo</label>
    <input type="text" class="form-control" name="indirizzo" id="indirizzo" value="<?=$indirizzo;?>" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
  <label for="sesso">Sesso: <select id="sesso" name="sesso">
      <option selected="true">Non specificato</option>
      <option>Uomo</option>
      <option>Donna</option>
    </select></label>
  </div>
  <div class="form-group">
    <label for="descrizione">Descrizione</label>
    <textarea name="descrizione" id="descrizione"><?=$descrizione; ?></textarea>
  </div>
  <span>I campi contrassegnati con * sono obbligatori!</span><br><br>
  <button type="submit" name="invia" class="btn btn-primary">Invia</button>
</form>
</body>

</html>
