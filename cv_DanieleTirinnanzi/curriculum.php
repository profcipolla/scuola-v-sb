<?php session_start() ?>

<?php
$isAutenticato = false;
if(isset($_SESSION["username"])){
  $username = $_SESSION["username"];
  $isAutenticato = true;
  //Informazioni utente
  $nome = $_SESSION["nome"];
  $cognome = $_SESSION["cognome"];

}

if(isset($_GET["logout"])){
  session_destroy();
  header("location:/cv_DanieleTirinnanzi/index.php");
}

?>
<html>

<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Curriculum</title>
<style>

  body{
    margin:20px;
  }
  .top{
    margin-left:-20px;
    margin-right:-20px;
    margin-top:-20px;
    padding:20px;
    background-color: #4bcffa;
    margin-bottom:1%;
  }
  .top #logout{
    margin-top: -5px;
    float:right;
  }
  .curriculumLeft{
    width:20%;
    background-color:#535c68;
    height:100%;
    text-align:left;
    float:left;
  }
  .curriculumRight{
    width:75%;
    float:left;
    margin-left:60px;
  }
  .imgutente{
    width: auto; 
    height: 150px;
  }
  .nominativo{
    margin-top:15px;
  }
  .curriculumLeftTop{
    background-color:white;
    text-align:center;
  }
  .sottotitolo{
    font-size:24px;
    font-family:Verdana;
  }
</style>
</head>

<body>

  <form method="get">
  <div class="top">
  <strong>Benvenuto <?=$username; ?></strong>
  <input type="submit" name ="logout" id = "logout" value="Logout">

  </div>
  </form>

  <?php
    if(!$isAutenticato){
      header("location:/cv_DanieleTirinnanzi/index.php");
    }
  ?>
  <center><h1>Curriculum utente</h1></center>
<div class = "curriculumLeft">
  <div class="curriculumLeftTop">
    <img class="imgutente" src="./utente.png"></img>
    <p class ="nominativo"><b><?=$nome;?> <?=$cognome;?></b></p>
    <hr>
  </div>
  <font color="white">
  <p class = "sottotitolo">Dati personali</p>
  <hr color="white">
  <p><b>Nome</b>: <?=$_SESSION["nome"]; ?></p>
  <p><b>Cognome</b>: <?=$_SESSION["cognome"]; ?></p>
  <p><b>Sesso</b>: <?=$_SESSION["sesso"]; ?></p>
  <p><b>Indirizzo</b>: <?php echo ($_SESSION["indirizzo"]=="" ? "Non specificato" : $_SESSION["indirizzo"]); ?></p>
  <p class="sottotitolo">Contatti</p>
  <hr color="white">
  <p>
  <p><b>Telefono</b>: <?=$_SESSION["telefono"]; ?></p>
  <p><b>Email</b>: <?=$_SESSION["email"]; ?></p>
  </p>
  </font>
  </div>
  <div class = "curriculumRight">
    <p class="sottotitolo">Profilo<p>
    <hr>

    <p>
    <?=$_SESSION["descrizione"];?>
    </p>
  </div>
</body>

 </html>
